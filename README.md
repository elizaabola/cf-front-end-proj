Q: What do you think of our current website and how would you improve it? Make your comments available on your Github or Bitbucket repo.

A:
First of all I think it's a great site. I don't have difficulty navigating through it. On top of all that, I really get a sense that you're a company that cares about its clients.

Speaking from my experience with helping my own clients build their sites, Social Media seems to be the best way to spread the word about companies. Having said that, I think it would be beneficial for the company to allow readers to easily share articles. While you do have social media icons for users to click and follow your social pages, I noticed that you didn't have social media share buttons on the articles. I just think it would give the company another way to get recognized.

Another small thing I noticed, the main navigation menus have a dropdown that repeats the menu title. For example, when you hover over "About Us" in the navigation bar you get a dropdown menu that repeats "About us" followed by the menu items. I think it's a little redundant to list the menu title twice. 

While we're on the topic of the navigation menu I see that the "Resources" page actually leads to, what seems to me, a blog page. I think it may be better to rename that menu title to "Blog." I just think it would be better understood that there are regularly posted articles there. 

One last thing, I think that the call to action button could stand out a little more. I think maybe saving the orange color for the "Apply Now" buttons exclusively would help.

These are just improvement ideas that I've gathered from my past opportunities. I'm curious to know if these ideas were included in the web design approach? I'd love to know more about your processes. 

Thank you for the opportunity and for considering me.